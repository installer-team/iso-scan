# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of ro.po to Romanian
# Romanian translation
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
# Eddy Petrișor <eddy.petrisor@gmail.com>, 2004, 2005, 2006, 2007, 2008, 2009, 2010.
# Jobava <jobaval10n@gmail.com>, 2018.
# Andrei Popescu <andreimpopescu@gmail.com>, 2018, 2020.
# Sergiu <adinfinitvm@wail.ch>, 2020.
#
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@debian.org>, 2004
# Andrei Popescu <andreimpopescu@gmail.com>, 2010.
# Eddy Petrișor <eddy.petrisor@gmail.com>, 2004, 2006, 2007, 2008, 2009.
# Free Software Foundation, Inc., 2000, 2001
# Lucian Adrian Grijincu <lucian.grijincu@gmail.com>, 2009, 2010.
# Mişu Moldovan <dumol@go.ro>, 2000, 2001.
# Tobias Toedter <t.toedter@gmx.net>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Ioan Eugen Stan <stan.ieugen@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: ro\n"
"Report-Msgid-Bugs-To: iso-scan@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:43+0100\n"
"PO-Revision-Date: 2024-12-04 12:00+0000\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <debian-l10n-romanian@lists.debian.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: utf-8\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../iso-scan.templates:1001
msgid "Scan hard drives for an installer ISO image"
msgstr "Scanarea discurilor dure pentru o imagine ISO de instalare"

#. Type: text
#. Description
#. :sl3:
#: ../iso-scan.templates:4001
msgid "Detecting hardware to find hard drives"
msgstr "Se detectează componentele pentru a găsi discurile dure"

#. Type: text
#. Description
#. :sl3:
#: ../iso-scan.templates:5001
msgid "Searching drives for an installer ISO image"
msgstr "Se caută pe discurile dure o imagine ISO cu programul de instalare"

#. Type: text
#. Description
#. :sl3:
#: ../iso-scan.templates:6001
msgid "Mounting ${DRIVE}..."
msgstr "Se montează ${DRIVE}..."

#. Type: text
#. Description
#. :sl3:
#: ../iso-scan.templates:7001
msgid "Scanning ${DRIVE} (in ${DIRECTORY})..."
msgstr "Se examinează ${DRIVE} (în ${DIRECTORY})..."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:8001
msgid "Do full disk search for installer ISO image?"
msgstr "Se caută pe întregul disc o imagine ISO cu programul de instalare?"

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:8001
msgid ""
"The quick scan for installer ISO images, which looks only in common places, "
"did not find an installer ISO image. It's possible that a more thorough "
"search will find the ISO image, but it may take a long time."
msgstr ""
"Examinarea rapidă, în căutarea unei imagini ISO cu programul de instalare, "
"care caută doar în locurile cele mai comune, nu a găsit o imagine ISO cu "
"programul de instalare. Este posibil ca o căutare mai amănunțită să găsească "
"imaginea ISO, dar poate dura mult mai mult timp."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:9001 ../iso-scan.templates:10001
msgid "Failed to find an installer ISO image"
msgstr "Nu s-a găsit o imagine ISO cu programul de instalare"

#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:9001
msgid ""
"No installer ISO images were found. If you downloaded the ISO image, it may "
"have a bad filename (not ending in \".iso\"), or it may be on a file system "
"that could not be mounted."
msgstr ""
"Nu s-a găsit nicio imagine ISO cu programul de instalare. Dacă ați descărcat "
"imaginea ISO, s-ar putea să nu aibă un nume bun (nu se termină în „.iso”), "
"sau ar putea fi pe un sistem de fișiere care nu a putut fi montat."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:9001 ../iso-scan.templates:10001
#: ../iso-scan.templates:11001
msgid ""
"You'll have to use an alternative installation method, select another device "
"to look for ISO image, or try again after you've fixed it."
msgstr ""
"Va trebui să folosiți o altă metodă de instalare, să selectați un alt "
"dispozitiv sau să încercați din nou după ce ați reparat imaginea ISO."

#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:10001
msgid ""
"While one or more possible ISO images were found, they could not be mounted. "
"The ISO image you downloaded may be corrupt."
msgstr ""
"Deși una sau mai multe imagini ISO posibile au fost găsite, ele nu au putut "
"fi montate. Imaginea ISO descărcată ar putea fi coruptă."

#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:11001
msgid "No installer ISO image found"
msgstr "Nu s-a găsit nici o imagine ISO cu programul de instalare"

#. Type: error
#. Description
#. :sl3:
#: ../iso-scan.templates:11001
msgid ""
"While one or more possible ISO images were found, they did not look like "
"valid installer ISO images."
msgstr ""
"Deși una sau mai multe imagini ISO posibile au fost găsite, ele nu par a fi "
"imagini ISO valide, cu programul de instalare."

#. Type: note
#. Description
#. :sl3:
#: ../iso-scan.templates:12001
msgid "Successfully mounted ${SUITE} installer ISO image"
msgstr "S-a montat cu succes imaginea ISO cu programul de instalare ${SUITE}"

#. Type: note
#. Description
#. :sl3:
#: ../iso-scan.templates:12001
msgid ""
"The ISO file ${FILENAME} on ${DEVICE} (${SUITE}) will be used as the "
"installation ISO image."
msgstr ""
"Fișierul ISO ${FILENAME} de pe ${DEVICE} (${SUITE}) va fi folosit ca imagine "
"ISO de instalare."

#. Type: select
#. Choices
#. :sl3:
#: ../iso-scan.templates:14001
msgid "All detected devices"
msgstr "Toate dispozitivele detectate"

#. Type: select
#. Choices
#. :sl3:
#: ../iso-scan.templates:14001
msgid "Enter device manually"
msgstr "Introduceți manual numele dispozitivului"

#. Type: select
#. Description
#. :sl3:
#: ../iso-scan.templates:14002
msgid "Device or partition to search for installation ISO(s):"
msgstr "Dispozitivul sau partiția pentru căutarea imaginilor de instalare ISO:"

#. Type: select
#. Description
#. :sl3:
#: ../iso-scan.templates:14002
msgid ""
"You can select a device, manually specify a non-detected device, or rescan "
"available devices (useful for slow USB devices)."
msgstr ""
"Puteți alege un dispozitiv, specifica manual un dispozitiv care nu a fost "
"detectat, sau să rescanați dispozitivele disponibile (util pentru "
"dispozitive USB lente)."

#. Type: string
#. Description
#. :sl3:
#: ../iso-scan.templates:15001
msgid "Device name:"
msgstr "Numele dispozitivului:"

#. Type: select
#. Choices
#. :sl3:
#: ../iso-scan.templates:16001
msgid "Full search"
msgstr "Căutare completă"

#. Type: select
#. Description
#. :sl3:
#: ../iso-scan.templates:16002
msgid "ISO file to use:"
msgstr "Fișier ISO de folosit:"

#. Type: select
#. Description
#. :sl3:
#: ../iso-scan.templates:16002
msgid ""
"One or multiple ISO files have been detected on the selected device(s). "
"Please choose which one you want to use, or ask for a more thorough search."
msgstr ""
"Unul sau mai multe fișiere ISO au fost detectate în dispozitivul(ele) "
"selectat(e). Alegeți pe care doriți să-l folosiți, sau cereți o căutare mai "
"a mănunțită."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:17001
msgid "Is ISO file ${FILENAME} the correct image for installation?"
msgstr "Este fișierul ISO ${FILENAME}, imaginea corectă pentru instalare?"

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:17001
msgid ""
"The ISO file ${FILENAME} on ${DEVICE} (${SUITE}, code ${CODENAME}, self-"
"described as '${DESCRIPTION}') will be used as the installation ISO image."
msgstr ""
"Fișierul ISO ${FILENAME} de pe ${DEVICE} (${SUITE}, nume în cod ${CODENAME}, "
"auto-descris „${DESCRIPTION}”) va fi folosit ca imagine ISO de instalare."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:17001
msgid ""
"If multiple ISO files exist on the same installer drive, you may select "
"which one you want to use."
msgstr ""
"Dacă există mai multe fișiere ISO pe același disc de instalare, puteți "
"selecta fișierul pe care doriți să-l folosiți."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:18001
msgid "Copy the ISO image into RAM before mounting it?"
msgstr "Doriți copierea imaginii ISO în RAM înainte de a o monta?"

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:18001
msgid ""
"There is enough available memory to be able to copy the ISO image into RAM."
msgstr ""
"Există suficientă memorie disponibilă pentru a putea copia imaginea ISO în "
"RAM."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:18001
msgid ""
"Choosing this option allows reusing the disk hosting the ISO image. If you "
"don't do it, the disk will be actively used to access the ISO image and it "
"can't be partitioned by the installer."
msgstr ""
"Opțiunea aceasta vă permite refolosirea discului cu imaginea ISO. Dacă nu o "
"folosiți, discul va fi folosit în mod activ pentru a accesa imaginea ISO și "
"acesta nu poate fi partiționat de programul de instalare."

#. Type: boolean
#. Description
#. :sl3:
#: ../iso-scan.templates:18001
msgid ""
"Note however that if you overwrite the disk containing the ISO image, you "
"should not reboot before the end of the installation as you will not be able "
"to restart the installer since the ISO image will be gone from the hard disk "
"and memory."
msgstr ""
"Rețineți totuși că, dacă suprascrieți discul care conține imaginea ISO, nu "
"trebuie să reporniți înainte de sfârșitul instalării, pentru că nu veți mai "
"putea reporni programul de instalare deoarece imaginea ISO va dispărea de pe "
"discul dur și din memorie."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl2:
#: ../load-iso.templates:1001
msgid "Load installer components from an installer ISO"
msgstr "Încarcă componentele programului de instalare de pe o imagine ISO"
